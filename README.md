# Task
HeadHunter and DreamJob companies work in collaboration, together they simplify and make the job search convenient for everyone. Now on the site hh.ru you can read reviews about the employer, which are collected by the dreamjob.ru service. Moderators decide whether or not to publish a review, there are 8 reasons for rejecting a review. You need to create a model that determines whether a review will be published and the reason for rejecting moderation.

# Algorithm Evaluation
The metric in this problem is Mean F1 score with averaging over samples. First, the F1 value is calculated separately for each review, and then it is averaged for the entire dataset.

N - total number of reviews
precision_i — failure reason prediction accuracy for the i-th review;
recall_i - completeness of predicting the reasons for the failure for the i-th review.

# Data
Participants will have to predict the reasons (encoded by numbers from 1 to 8) for which the review does not pass moderation. There can be several reasons why the review did not pass moderation. If the review is moderated, then 0 should be sent instead of the reason for the refusal. The value 0 is allowed to be combined with reasons for the refusal.
